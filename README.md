# Repo audit example

## Test
1. Open a terminal at the project folder.
2. `./node_modules/.bin/truffle develop`
3. `./node_modules/.bin/truffle test` - run tests and check gas usage

### Test coverage
1. Open a terminal at the project folder.
2. `./node_modules/.bin/solidity-coverage` - run tests and check the coverage

