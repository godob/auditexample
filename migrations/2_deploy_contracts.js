var BadToken = artifacts.require("./BadToken.sol");

module.exports = function(deployer, network, accounts) {
  let name = "badToken";
  let symbol = "BAD";
  let decimals = 18;
  let totalSupply = 1000;

  deployer.deploy(BadToken, name, symbol, decimals, totalSupply);
};
