# REPORT SMART CONTRACTS AUDIT RESULTS

## Vulnerability Level
* Low severity – A vulnerability that does not have a significant impact on the use of the contract and is probably subjective.
* Medium severity – A vulnerability that could affect the desired outcome of executing the contract in certain scenarios.
* High severity – A vulnerability that affects the desired outcome when using a contract, or provides the opportunity to use a contract in an unintended way.
* Critical severity – A vulnerability that can disrupt the contract functioning in a number of scenarios, or creates the risk that the contract may be broken.

## Main results
```
--------------------|----------|----------|----------|----------|
File                | Critical |     High |   Medium |      Low |
--------------------|----------|----------|----------|----------|
  BasicToken.sol    |        - |        - |        1 |        - |
  ERC20.sol         |        - |        - |        - |        - |
  ERC20Basic.sol    |        - |        - |        - |        - |
  MintableToken.sol |        - |        - |        - |        1 |
  Ownable.sol       |        - |        - |        - |        - |
  StandardToken.sol |        - |        - |        2 |        1 |   
  BadToken.sol      |        - |        - |        - |        - |  
  SafeMath.sol      |        - |        - |        - |        1 |
  Genaral Comments  |        - |        - |        2 |        4 |
--------------------|----------|----------|----------|----------|
All files           |        - |        - |        5 |        7 |
--------------------|----------|----------|----------|----------|
```

## Evaluation of BasicToken.sol
### Medium Severity
* This file contains the `transfer` function, which has the address as one of the input parameters, but don’t check the address for a null value (in the Ethereum virtual machine, omitted values are interpreted as null values), which means that token loss is possible when using this function (they will be sent to the address 0x0). Recommendation: Implement a null address check.

## Evaluation of ERC20.sol
This file meets modern security requirements and does not require changes.

## Evaluation of ERC20Basic.sol
This file meets modern security requirements and does not require changes.

## Evaluation of MintableToken.sol
### Low Severity
* Function `mint` checks the address for a null value, but it compares the address data type with a null value. At this time, implicit type conversion is being performed by the compiler, but we recommend to do this explicitly. Recommendation: Before 0, add an explicit conversion to the address type (address(0)).

## Evaluation of Ownable.sol
This file meets modern security requirements and does not require changes.

## Evaluation of SafeMath.sol
### Low Severity
* The `constant` identifier has been deprecated, and now the `view` or `pure` identifier is used instead. However, the `constant` identifier is currently an alias of the `view` identifier. But functions can be declared `pure` in which case they promise not to read from or modify the state. Recommendation: Replace the `constant` identifier with the `pure` identifier for all functions.

## Evaluation of StandardToken.sol
### Medium Severity
* This file contains the `transferFrom` function, which has the address as one of the input parameters, but don’t check the address for a null value (in the Ethereum virtual machine, omitted values are interpreted as null values), which means that token loss is possible when using this function (they will be sent to the address 0x0).
* Recommendation: Implement a null address check.
### Low Severity
* This file contains the "approve" function, which has a check to deflect attack vectors for the `Approve/TransferFrom` functions. However, there are no functions to increase or decrease the amount of "approved" tokens, which leads to less flexibility in the use of tokens (the number of "approved" tokens must always be reduced to 0 first, which means there are two function calls instead of one). Recommendation: To reduce overhead costs, implement `increaseApproval` and `decreaseApproval` functions where this is appropriate.

## Evaluation of BadToken.sol
This file meets modern security requirements and does not require changes.

## General Comments
### Medium Severity
* Current code is written for old versions of solc. Use latest version of Solidity. Recommendation: Use solc version 0.4.24.
* There is no protection from transferring tokens to the address of the contract (this situation is quite common). And since the contract does not provide any functions for handling tokens on the contract balance, any tokens accidentally transferred to the contract address will be lost. Recommendation: Add protection function for this case.

### Low Severity
* Defining constructors as functions with the same name as the contract is deprecated in solidity version 0.4.22. Use "constructor(...) { ... }" instead. Recommendation: Update “Ownable.sol” and “BadToken.sol” contracts.
* For consistency in calculations, the SafeMath library could be used everywhere. Recommendation: Use SafeMath library for all arithmetic actions.
* Invoking events without `emit` prefix is deprecated in Solidity since version 0.4.21. Recommendation: Add key word `emit` before all events.
* Contracts should be deployed with the same compiler version and flags that they have been tested the most with. Locking the pragma helps ensure that contracts do not accidentally get deployed using, for example, the latest compiler which may have higher risks of undiscovered bugs. Contracts may also be deployed by others and the pragma indicates the compiler version intended by the original authors. Recommendation: Lock pragmas to specific compiler version.

## Coverage
```
--------------------|----------|----------|----------|----------|----------------|
File                |  % Stmts | % Branch |  % Funcs |  % Lines |Uncovered Lines |
--------------------|----------|----------|----------|----------|----------------|
 contracts/         |    38.64 |    16.67 |       40 |    37.78 |                |
  BadToken.sol      |      100 |      100 |      100 |      100 |                |
  BasicToken.sol    |    85.71 |       50 |    66.67 |    85.71 |             23 |
  ERC20.sol         |      100 |      100 |      100 |      100 |                |
  ERC20Basic.sol    |      100 |      100 |      100 |      100 |                |
  MintableToken.sol |        0 |        0 |        0 |        0 |... 18,19,20,21 |
  Ownable.sol       |       20 |        0 |    33.33 |    16.67 | 13,14,18,19,20 |
  SafeMath.sol      |       50 |    33.33 |       50 |       50 | 10,11,12,17,19 |
  StandardToken.sol |        0 |        0 |        0 |        0 |... 46,47,48,58 |
--------------------|----------|----------|----------|----------|----------------|
All files           |    38.64 |    16.67 |       40 |    37.78 |                |
--------------------|----------|----------|----------|----------|----------------|
```

## Gas usage
```
·-----------------------------------------------------------------|----------------------------·
|                               Gas                               ·  Block limit: 6721975 gas  │
········································|·························|·····························
|  Methods                              ·       21 gwei/gas       ·       292.98 usd/eth       │
··················|·····················|·······|·······|·········|·············|···············
|  Contract       ·  Method             ·  Min  ·  Max  ·  Avg    ·  # calls    ·  usd (avg)   │
··················|·····················|·······|·······|·········|·············|···············
|  MintableToken  ·  mint               ·    -  ·    -  ·      -  ·          0  ·           -  │
··················|·····················|·······|·······|·········|·············|···············
|  Ownable        ·  transferOwnership  ·    -  ·    -  ·      -  ·          0  ·           -  │
··················|·····················|·······|·······|·········|·············|···············
|  StandardToken  ·  approve            ·    -  ·    -  ·      -  ·          0  ·           -  │
··················|·····················|·······|·······|·········|·············|···············
|  StandardToken  ·  transfer           ·    -  ·    -  ·  51530  ·          1  ·        0.32  │
··················|·····················|·······|·······|·········|·············|···············
|  StandardToken  ·  transferFrom       ·    -  ·    -  ·      -  ·          0  ·           -  │
·-----------------|---------------------|-------|-------|---------|-------------|--------------·

```

## Conclusion     
Serious vulnerabilities were not detected. Also this contract has minor recommendations that may affect certain functions or cause inconveniences when using the contract. However, they will not prevent the contract from functioning as intended. Contract has partially tests.

## Tools Used      
The audit of the contract code was conducted using the Remix IDE (http://remix.ethereum.org).
The Solidity compiler version used was 0.4.24 (the most recent stable version).

### Other tools:

#### Static Analysis
1) Mythril - Reversing and bug hunting framework for the Ethereum blockchain
2) Oyente - Analyze Ethereum code to find common vulnerabilities.
3) Solgraph - Generates a DOT graph that visualizes function control flow of a Solidity contract and highlights potential security vulnerabilities.

#### Test Coverage
1) solidity-coverage - Code coverage for Solidity testing (https://github.com/sc-forks/solidity-coverage)

#### Gas udage
1) ETH gas reporter (https://github.com/cgewecke/eth-gas-reporter)

#### Linters
Linters improve code quality by enforcing rules for style and composition, making code easier to read and review.
1) Solium (https://github.com/duaraghav8/Solium)
2) Solhint (https://github.com/protofire/solhint)