pragma solidity ^0.4.18;

import "./StandardToken.sol";
import "./Ownable.sol";

contract MintableToken is StandardToken, Ownable {
    event Mint(address indexed to, uint256 amount);

    /**
     * @dev Function to mint tokens
     * @param _to The address that will receive the minted tokens.
     * @param _amount The amount of tokens to mint.
     * @return A boolean that indicates if the operation was successful.
     */
    function mint(address _to, uint256 _amount) onlyOwner public returns (bool) {
        require(_to != 0x0);
        totalSupply_ = totalSupply_ + _amount;
        balances[_to] = balances[_to] + _amount;
        Mint(_to, _amount);
        Transfer(0, _to, _amount);
        return true;
    }
}
