pragma solidity ^0.4.18;

import "./MintableToken.sol";

contract BadToken is MintableToken {
    string public name;
    string public symbol;
    uint8 public decimals;

    function BadToken(string _name, string _symbol, uint8 _decimals, uint _totalSupply) public {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;
        totalSupply_ = _totalSupply;
        balances[owner] = _totalSupply;
    }
}